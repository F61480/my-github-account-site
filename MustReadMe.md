# MustReadMe #

### Who am I? ###

I am second year student in B.Sc. CS at NBU in Sofia, Bulgaria. My grade for academic years 1 and 2 is "B".
As of Feb 2017, I have studied primarily (for better or worse) C++. My next undertakings are Java, Python and Android.

### What is this repository for? ###

This repository is to link my GitHub account and site to my Bitbucket account, which are WIP as of Feb 2017. I solely use my Bitbucket account to upload code written for algorithm / DS practice 
or code from HackerRank challanges / contests. My projects that are covering RL scenarious / problems / projects, can be located at GitHub. 

### Who do I talk to? ###

Anyone who is passionate about programming.
(And isn't a creep)

### My GitHub account and website: ###

https://github.com/TiMladenov
https://timladenov.github.io/

### My HackerRank profile: ###

https://www.hackerrank.com/tihomir007